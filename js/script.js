'use strict';

let photoDescriptionSliderOriginWidth = 0;

function SetTheImageWidthRatio(origin, actual) {
    return actual / origin;
}

function setWidthImagesContainer(elem) {

    let actualWidth = 0;

    $('.calc_cars_img img').each(function () {

        let wrap = $('.wrapper').width();

        if(wrap > 737){
            $(this).width(elem.width()/2);
            actualWidth = $(this).width()
        }

        if(wrap <= 737){
            $(this).width(elem.width());
            actualWidth = $(this).width()
        }

    });

    return act_width;
}


$(window).resize(function () {

    $('.sliderRewiewContent').width($('.sliderActiveArea').width());

    $('.item').width($('.sliderPhotoActiveArea').width());

    /* $('.sliderContent > img').width();
 */



});

$(document).ready(function () {

    $('.sliderRewiewContent').width($('.sliderActiveArea').width());

    $('.item').width($('.sliderPhotoActiveArea').width());

    let sliderDocsContentWidth = $('.sliderContent > img').width();

    let rewiewSliderContentWidth = $('.sliderRewiewContent').width();

    let photoItemContainerWidth = $('.item').width();



    $(document).on('click', '.button', function () {
        $('.feedback_hidden_block').addClass('active');
        $('.form_container').addClass('active');
        $('.form_container.active').css({'top': $(document).scrollTop()});
    });

    $(document).on('click', '.close_hidden_block', function () {
        $('.feedback_hidden_block').removeClass('active');
        $('.form_container').removeClass('active');
    });

    $(document).on('click', '.hamburger', function () {
        $(this).addClass('active');
        $('header nav').addClass('mobile');
        $('.close_menu').addClass('active');
    });

    $(document).on('click', '.close_menu, .mobile ul li a', function () {
        $('.hamburger').removeClass('active');
        $('header nav').removeClass('mobile');
        $('form_container').removeClass('active');
    });

    $(document).on('click', 'nav ul li a', function () {
        let hash = $(this).attr('href');
        $('html, body').animate({scrollTop:$(hash).position().top - 100}, 400);
    });

    $(document).on('click', '.slide_back', function () {
        $("body,html").animate({scrollTop: $('html').offset().top - 50}, 400);
    });

    $(document).on('scroll', function () {
        let html_top = $('html').scrollTop();
        //console.log(html_top);
        if(html_top >= 400) $('.slide_back').addClass('active');
        else $('.slide_back').removeClass('active');

    });

    $(document).on('click', '.feedback_hidden_block.active', function () {
        $(this).removeClass('active');
        $('.form_container').removeClass('active');
    });

    $(document).on('click', '.form_container', function (e) {
        e.stopPropagation();
    });

    $(document).on('click', '.leftSlideReview, .rightSlideReview', function () {
        sertificationSlider($(this), 'leftSlideReview', 'rightSlideReview', '.sliderContentRewiew', rewiewSliderContentWidth);
    });

    $(document).on('click', '.leftSlidePhoto, .rightSlidePhoto', function () {
        sertificationSlider($(this), 'leftSlidePhoto', 'rightSlidePhoto', '.sliderPhotoContent', photoItemContainerWidth);
    });

    autoslider('.sliderPhotoContent', photoItemContainerWidth, 3500);

    autoslider('.sliderContentRewiew', rewiewSliderContentWidth, 4500);


    $(document).on('click', '.sertificateBlock',  function () {
        renderFullScreenSertificat($(this).children('img').attr('data-image-full-screen'));
    });

    $(document).on('click', '.darkBg, .closePopUp', function (e) {
        e.stopPropagation();
        $('.darkBg').remove();
    });

    function renderFullScreenSertificat(imagePath) {

        let darkBlock =  $('<div>', {'class': 'darkBg'});
        $('body').append(darkBlock);

        let image = $('<img>', {'src' : imagePath });
        darkBlock.append(image);

        let close = $('<span>', {'class': 'closePopUp'});
        darkBlock.append(close);

    }



    $("input[name='phone']").mask("+375 (99) 999-99-99");

    $("input").removeClass("error-input");

    $(document).on('click', '.send', function (e) {
        e.preventDefault();
        sendmailForm();
     });

    $(document).on('keypress', '#name, #phone, #formText', function (e) {
        if(e.which === 13 || e.which === 9) {

            if($(this).attr('id') === 'name') {
                $("#phone")[0].focus();
            }

            if($(this).attr('id') === 'phone') {
                $("#formText")[0].focus();
            }

        }
    });
    
});


function sertificationSlider(that, classArrowLeft, classArrowRight, animateContainer, sliderContentWidth) {

    let position = $(animateContainer).position();

    if(that.hasClass(classArrowLeft)) {

        if(position.left <= -($(animateContainer).width() - sliderContentWidth)) {
            position.left = 0;
        }else {
            position.left-= sliderContentWidth;
        }
        $(animateContainer).animate({'left': position.left + 'px'});
        return false;
    }

    if(that.hasClass(classArrowRight)) {
        if(position.left >= 0) {
            position.left = -($(animateContainer).width() - sliderContentWidth);
        }else {
            position.left+= sliderContentWidth;

        }
        $(animateContainer).animate({'left': position.left + 'px'});
        return false;
    }
}

function autoslider(animateContainer, sliderContentWidth, time) {
    let position = $(animateContainer).position();
    console.log(position)
    let autoSlide = setInterval(function () {
        if(position.left <= -($(animateContainer).width() - sliderContentWidth)) {
            position.left = 0;
        }else {
            position.left-= sliderContentWidth;
        }
        $(animateContainer).animate({'left': position.left + 'px'});
    },time);
}

function sendmailForm() {

    var phoneVal = $("#phone").val();

    if (validatePhone(phoneVal) === true) {

        $.ajax({
            type: 'POST',
            url: 'mail.php',
            data: $('form').serialize(),
            success: function () {
                $('.feedback_hidden_block').removeClass('active');
                $('.form_container ').removeClass('active');
                $("#phone").val('');
                $("#name").val('');
                showMessage(true, 'Спасибо за заявку. Менеджер свяжется с вами в течении 5 минут.');
            },
            error: function () {
                showMessage(false, 'Ошибка, попробуйте еще раз.');
            }
        });

    } else {
        $("#phone").addClass("error-input");
    }
}

function showMessage(flag, message) {

    var showMessage = $('.showMessage');

    if( !flag ){
        showMessage.css({'background': '#f7b4b4', 'color': '#fff'});
    }

    showMessage.text(message);
    showMessage.addClass('active');
    $('.showMessage.active').css({'top': $(document).scrollTop() + 100});




    setTimeout(function () {
        showMessage.removeClass('active');
        showMessage.text('');
    },1500)

}

function validatePhone($phone){
    var phoneMask = $phone.split("").slice(6,8);
    var codePhone = phoneMask.join("");

    if(codePhone == "29" || codePhone == "33" || codePhone == "25" || codePhone == "44"){
        return true;
    }

    else{
        return false;
    }
}




